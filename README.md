# bitboxio.react
This is a starter project for a front end that will run on ios, andriod as well as the web (possibly desktop).

If your working on a mac with a later version of node (10/12) and npm (>6) then there are a few bugs regarding runing the react js dev server. First install yarn (npm install -g yarn) and create your project... npx react-create-app myapp.

Next your can do this manually but it's easier to open package.json and edit "scripts" -> "start" : "HOST=localhost react-scripts start". This addresses a env HOST var that seem to be on later macs.

Finally getting the andriod and ios to build and run can be a bit of hassle on the first build. For iOS just open xcode and build so you can better see warning and/or errors. For Andriod I had to drop a local.properties file that points to the Andriod SDK.

More to come...
*HAPPY HACKING!*

## TODO
- Create Docker Image for python api and config project to use mocks/stubs, etc.
- RnD on db sync with devices (relm, sqlite, pouchdb, mongo mini, etc.). IMO it should be able to working with RDBMS or NoSQL docstore that supporst json docs (or python ordered dictionaries :)).
